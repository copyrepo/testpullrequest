CREATE SCHEMA IF NOT EXISTS `activation`;
CREATE TABLE `activation`.`sim_profile` (
  `sim_profile_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_code` CHAR(2) NOT NULL,
  `source_ip` INT UNSIGNED NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL, /* 1 - activated, 2 - incomplete, 3 - invalid, 4 - activation_not_allowed  */
  `csAgent` VARCHAR(255) NOT NULL DEFAULT '',
  `retailer` VARCHAR(255) NOT NULL DEFAULT '',
  `iccid` DECIMAL(4) ZEROFILL NOT NULL,
  `msisdn` BIGINT UNSIGNED NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sim_profile_id`),
  UNIQUE INDEX `msisndx_sim_profile_id` (`msisdn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `activation`.`address` (
  `sim_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `post_code` VARCHAR(16) NOT NULL DEFAULT '',
  `address_line1` VARCHAR(255)  NOT NULL DEFAULT '',
  `address_line2` VARCHAR(255)  NOT NULL DEFAULT '',
  `address_line3` VARCHAR(255)  NOT NULL DEFAULT '',
  `city` VARCHAR(255)  NOT NULL DEFAULT '',
  `county` VARCHAR(255)  NOT NULL DEFAULT '',
  `house_number` VARCHAR(255) NOT NULL DEFAULT '',
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sim_profile_id`),
  CONSTRAINT `fk_address_profile` FOREIGN KEY (`sim_profile_id`) REFERENCES `sim_profile`(`sim_profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `activation`.`personal_data` (
  `sim_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `title` VARCHAR(10)  NOT NULL DEFAULT '',
  `first_name` VARCHAR(255) NOT NULL DEFAULT '',
  `last_name` VARCHAR(255) NOT NULL DEFAULT '',
  `date_of_birth` DATE,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sim_profile_id`),
  CONSTRAINT `fk_personal_data_profile` FOREIGN KEY (`sim_profile_id`) REFERENCES `sim_profile`(`sim_profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `activation`.`contact_info` (
  `sim_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `primary_email` TEXT,
  `alternate_phone` LONG,
  `alternate_email` TEXT,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sim_profile_id`),
  CONSTRAINT `fk_contact_info_profile` FOREIGN KEY (`sim_profile_id`) REFERENCES `sim_profile`(`sim_profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `activation`.`identity_document` (
  `sim_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `type` TINYINT UNSIGNED, /* 1 - passport, 2 - driver license, 3 - id card, 4 - birth-certificate, */
  `document_number` VARCHAR(50) NOT NULL DEFAULT '',
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sim_profile_id`),
  CONSTRAINT `fk_identity_document_profile` FOREIGN KEY (`sim_profile_id`) REFERENCES `sim_profile`(`sim_profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
