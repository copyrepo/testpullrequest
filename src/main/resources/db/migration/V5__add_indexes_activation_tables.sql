CREATE INDEX `idx_first_name`  ON personal_data (first_name, last_name);
CREATE INDEX `idx_source_ip`  ON sim_profile (source_ip);
CREATE INDEX `idx_country_code`  ON sim_profile (country_code, created_at);
