CREATE TABLE `activation`.`failed_activations` (
  `failed_activations_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_code`      CHAR(2)             NOT NULL,
  `source_ip`         INT UNSIGNED        NOT NULL,
  `csAgent`           VARCHAR(255)        NOT NULL DEFAULT '',
  `retailer`          VARCHAR(255)        NOT NULL DEFAULT '',
  `iccid`             DECIMAL(4) ZEROFILL NOT NULL,
  `msisdn`            BIGINT UNSIGNED     NOT NULL,

  `post_code`         VARCHAR(16)         NOT NULL DEFAULT '',
  `address_line1`     VARCHAR(255)        NOT NULL DEFAULT '',
  `address_line2`     VARCHAR(255)        NOT NULL DEFAULT '',
  `address_line3`     VARCHAR(255)        NOT NULL DEFAULT '',
  `city`              VARCHAR(255)        NOT NULL DEFAULT '',
  `county`            VARCHAR(255)        NOT NULL DEFAULT '',
  `house_number`      VARCHAR(255)        NOT NULL DEFAULT '',

  `title`             VARCHAR(10)         NOT NULL DEFAULT '',
  `first_name`        VARCHAR(255)        NOT NULL DEFAULT '',
  `last_name`         VARCHAR(255)        NOT NULL DEFAULT '',
  `date_of_birth`     DATE,

  `primary_email`     TEXT,
  `alternate_phone`   LONG,
  `alternate_email`   TEXT,

  `identity_type`     TINYINT UNSIGNED, /* 1 - passport, 2 - driver license, 3 - id card, 4 - birth-certificate, */
  `document_number`   VARCHAR(50)         NOT NULL DEFAULT '',
  `failure_reason`    VARCHAR(255)        NOT NULL,
  `created_at`        TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`        TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`failed_activations_id`),
  UNIQUE INDEX `msisndx_failed_activations_id` (`msisdn`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
