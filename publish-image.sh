#!/bin/bash
set -xe

source image-version.sh

PUBLISH_FILE_PREFIX="publish-image"
TAG=$(git describe --always --tag)

ENV=${1}
if [ "${ENV}" == "testing" ]; then
  CLUSTER_NAME=arabel-talk
elif [ "${ENV}" == "prod" ]; then
  CLUSTER_NAME=lebara-talk
else
  echo "Unknown environment [${ENV}]"
  exit 1;
fi

ECR_REPO="123203969087.dkr.ecr.eu-west-1.amazonaws.com/${IMAGE_NAME}"

docker tag ${IMAGE_NAME}:${IMAGE_VERSION} ${ECR_REPO}:${IMAGE_VERSION}-${TAG}
docker tag ${IMAGE_NAME}:${IMAGE_VERSION} ${ECR_REPO}:latest

$(aws ecr get-login --region eu-west-1)
docker push ${ECR_REPO}:${IMAGE_VERSION}-${TAG}
docker push ${ECR_REPO}:latest

TASK_NAME=${IMAGE_NAME}-${ENV}

# Create a new task definition for this build
sed -e "s;%IMAGE_VERSION%;${IMAGE_VERSION};g;s;%IMAGE_NAME%;${IMAGE_NAME};g;s;%TAG%;${TAG};g;s;%ENV%;${ENV};g" ${PUBLISH_FILE_PREFIX}-${ENV}.json > ${IMAGE_NAME}-${IMAGE_VERSION}-${TAG}.json
aws --region=eu-west-1 ecs register-task-definition --cli-input-json file://${IMAGE_NAME}-${IMAGE_VERSION}-${TAG}.json

# Update the service with the new task definition and desired count
TASK_ARN=`aws --region=eu-west-1 ecs describe-task-definition --task-definition ${TASK_NAME} | awk -F\" '/taskDefinitionArn/ {print $4}'`
DESIRED_COUNT=`aws --region=eu-west-1 ecs describe-services --cluster ${CLUSTER_NAME} --service ${IMAGE_NAME} | egrep desiredCount | tail -n 1 | tr "/" " " | awk '{print $2}' | sed 's/,$//'`
if [[ ${DESIRED_COUNT} = "0" ]]; then
    DESIRED_COUNT="1"
fi

aws --region=eu-west-1 ecs update-service --cluster ${CLUSTER_NAME} --service ${IMAGE_NAME} --desired-count ${DESIRED_COUNT} --task-definition ${TASK_ARN}
