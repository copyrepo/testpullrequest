#!/bin/bash
set -xe

source image-version.sh

(
# test image:
docker-compose up -d
# wait for the migration to complete
EXIT_CODE=$(docker wait `docker ps|grep $IMAGE_NAME-provisioner|awk '{print $1}'`)

docker-compose down

# Fail on non-zero exit code.
if [ $EXIT_CODE != 0 ]; then
  echo "Failed"
  exit $EXIT_CODE
else
  echo "Passed"
fi

) || docker-compose down
