FROM maven:3-jdk-8-alpine

MAINTAINER Rostyslav Dyyak <rdyyak@n-ix.com>

ENV DB_ADMIN_USER root

VOLUME /tmp
ADD . .

RUN curl -OL https://github.com/jwilder/dockerize/releases/download/v0.2.0/dockerize-linux-amd64-v0.2.0.tar.gz
RUN tar -C /usr/local/bin -xzvf dockerize-linux-amd64-v0.2.0.tar.gz

ENTRYPOINT ["./docker-entrypoint.sh"]
