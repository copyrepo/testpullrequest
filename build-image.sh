#!/bin/bash
set -xe

source image-version.sh
# build image:
docker build -t $IMAGE_NAME:$IMAGE_VERSION -t $IMAGE_NAME:latest .
