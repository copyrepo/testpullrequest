#!/usr/bin/env bash

dockerize -wait tcp://$DB_HOST:3306

mvn clean compile -P flyway \
  -Dflyway.user="$DB_ADMIN_USER" \
  -Dflyway.password="$DB_ADMIN_PASSWORD" \
  -Dflyway.url=jdbc:mysql://$DB_HOST:3306 \
  -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn
